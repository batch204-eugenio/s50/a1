import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
//import bootsrap css, copied from bootsrap.
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



//Don't //
//------------------------------------------
//JSX format doesn't need a $ sign like template literal.
//JSX (JavaScript + XML), is an extension of JS that let's us create objects which will be then compiled and added as HTML elements.
//START, Checking if React is working-------------------------------------

//*********
// const name = "John Smith";
// let element = <h1>Hello, {name}</h1> 
// //------------------------------------------

// //changing the name from 'John Smith' to 'Jane Smith'
// //create a user object
// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }
// //create a function that will use the 'user' as parameter.
// function formatName(profile) {
//   return profile.firstName +' '+profile.lastName;
// }
// //formatName(), no need to invoke like this, root.render will server like an invocation.
// element = <h1>Hello, {formatName(user)}</h1> //reassignment.
// //------------------------------------------

// root.render(element); //root.render() invoke the const element and render/display our elements and display in our HTML.
//*********