//copied from bootstrap components under navbar
//Option 1 Long method
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

//Option 2 Short method
// related to SINGLE state (useState) that all our components will see, access and handle if the user is logged in or not. 
import React from 'react';
import {useContext} from 'react'
//end SINGLE
import {Container, Nav, Navbar, NavDropdown} from "react-bootstrap"

//for the route
import {Link, NavLink} from 'react-router-dom'
// related to SINGLE state (useState) that all our components will see, access, remember and handle if the user is logged in or not. 
//this contains now our {user, setUser} 
import UserContext from '../UserContext';

//----------------------------------------------
//manually typed
export default function AppNavBar() { //use PascalCase
  // related to SINGLE state (useState) that all our components will see, access and handle if the user is logged in or not. 
  // A context object such as our UserContext can ge 'opened with reast's useContext hook
  //this is responsible to open 'UserContext', this is like a box cutter.
  //this 'useContext()' is another hook aside from 'useState' & 'useEffect'
  const {user, setUser} = useContext(UserContext);
  //end Single state
	return( 						  //return is necessary
//----------------------------------------------
//copied from bootstrap components under navbar
		  <Navbar bg="light" expand="lg">  
      <Container>
{/*      Do not uncomment this
        <Navbar.Brand href="#home">Zuitt</Navbar.Brand>*/}
      {/*Link component is the right way to navigate, Link disguised as a Navbar.Brand*/}
        <Navbar.Brand as={Link} to = "/">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto"> 
{/*         Do not uncomment this.
           <Nav.Link href="#home">Home</Nav.Link>*/}
            <Nav.Link as={NavLink} to = "/">Home</Nav.Link>
            <Nav.Link as={NavLink} to = "/courses">Courses</Nav.Link>

            {/*related to SINGLE state useContext*/}
            {(user.email !== null) 
                ?
                <Link className="nav-link" to="/logout">Logout</Link>
                :
               <> 
                <Link className="nav-link" to="/login">Login</Link> 
                <Link className="nav-link" to="/register">Register</Link> 
               </>
            }


{/*            <Nav.Link as={NavLink} to = "/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to = "/register">Register</Nav.Link>        */}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
//copy end    
//----------------------------------------------    
	)
}