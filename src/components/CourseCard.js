import { Button, Col, Card } from 'react-bootstrap';
import { useState, useEffect } from 'react'; //state hooks

//option 1
// export default function CourseCard(props) {
//     console.log(props.courseProp);

//option 2 w/ destructuring.
//Desctructuring is done in the parameter to retrieve the courseProp.
//Parent is Courses child is CourseCard
export default function CourseCard({courseProp}) {
    // console.log(courseProp);

    const {name, description, price} = courseProp;
    //use the 'state hook' for this component to be able to store its state(estado)
    //'states' are used to keep track of info related to 'individual/SINGLE components' 
    /*Syntax:
        const [getter, setter] = useState(initialGetterValue)
    */
    //count/state:getter:viewCount  and  setCount:setter:changeCount>store to > useState(w/ 0 as default)
    
    //when a component mounts (loads for the 1st time), any associated states will undergo a state change from null to the given initial/defualt state
    //e.g. count below goes from null to 0, since we used 0 as our initial state
    //state can be numbers, arrays, string, etc.
    const [count, setCount] = useState(0);
    //ACTIVITY 51-----------------------------
    const [seats, setSeats] = useState(30);
    //ACTIVITY END----------------------------
     //Using the 'state hook' RETURNS an array w/the first element being a value and the 2nd element as a function that's used to change the value of the first element.
     
    // console.log(useState(0));
    // console.log(useState(30));

    //console.log prints what 'count:getter' views not the 'setCount:setter'
    //'coun't' and 'setCount' are user defind
    //Function that keeps track for example enrollees for a course.
    //By default JS is synchronous, it executes code from top of the file all the way to the bottom and will wait or the completion of one expression before it proceeds to the next
    //'setter FX' for useStates are asynchronous allowing it to execute separately from other codes in the program.
    //'setCount FX' is being executed while the console.log is already being completed, resulting in the console to be behind by one count.
    //si 'getter' is to view yung value nung use state po natin, while si 'setter' is to change po yung initial value sa may loob nung useState() natin.
    
    //function that will count the enrollees, change button to onClick={enroll}
    //we added <Card.Text>Enrollees: {count}</Card.Text>
    //onClick is an event that will be triggered when clicked.
    //each time enrollee clicks it is set equal to 1.
    // function enroll() {
    //     setCount(count + 1);
    //     console.log("Enrollees:" + count)

    // }




    //Zuitt answer---------------------------------------------------------
    function enroll() {
        // if (seats > 0) {
            setCount(count + 1);
            // console.log("Enrollees: " + 1);
            setSeats(seats - 1);
            // console.log("Seats " + seats)
        }
        // else {
        //     alert("Sorry, no more seats available!");
        // }
    // }    
    //Zuitt answer end-----------------------------------------------------
    //1 useEffect makes any give code block hapen when a state changes AND when a component first mounts(such as on initial page load)
    //useEffect happens when we mount or load the page.
    //important in getting data.
    /*2 Syntax:
    useEffect(()=> {
        code to be executed/console logged.
    }, [state(s) to monitor or getter and setter])
    */
    //in the example below, since count and seats are in the array of our useState, the code block/console.log will execute whenever those state change
    //3 if the array is blank [], no count and seats, the code will be executed on component mount ONLY
    //4 do not let the array be empty or blank.
	useEffect(() => {
        if (seats === 0) {alert('No more seats available')}
        console.log('state changed')
    }, [count, seats]) //this array is monitoring count and seats, any changes will trigger the code block/console.log('state changed')

    return (
		    <Col>
                <Card className="mb-2 p-3">
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>PhP {price}</Card.Text>
                        <Card.Text>Enrollees: {count}</Card.Text>        
                        <Card.Text>Seats: {seats}</Card.Text> 
                        <Button variant="primary" onClick={enroll}>Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
	)
}




    //Activity Start my answer 51------------------------------------------------------
    
    //    function enroll() {
    //     setCount(count + 1);
    //     setCountSeat(countSeat - 1)
    //     if (countSeat === 0 && count === 30) {
    //         setCount (count);
    //         setCountSeat(countSeat);
    //         alert("Sorry, no more seats available!");
    //         console.log("Enrollees: " + count);
    //         console.log("Seats: " + countSeat);
    //     }    
    // }

    // <Card.Text>Seats Available: {countSeat}</Card.Text> 
    //Activity End----------------------------------------------------------