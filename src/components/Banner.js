//creating a literal banner.
import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom'

//refactoring reference Home.js
export default function Banner({dataProp}) {
	const {title, content, destination, label} = dataProp
	return(
		//using CRC rule.
		//class name is the same as class like an id
		<Row>
			<Col className= "p-5 text-center"> 
				<h1>{title}</h1>
				<p>{content}</p>
				<Link className="btn btn-primary" to={destination}>{label}</Link>
			</Col>
		</Row>
	)
}









//ORIGINAL before refactoring
// export default function Banner() {
// 	return(
// 		//using CRC rule.
// 		//class name is the same as class like an id
// 		<Row>
// 			<Col className= "p-5 text-center"> 
// 				<h1>Zuitt Coding Bootcamp</h1>
// 				<p>Opportunities for everyone, everywhere.</p>
// 				<Button variant="primary">Enroll now!</Button>
// 			</Col>
// 		</Row>
// 	)
// }