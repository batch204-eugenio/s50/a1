//step 2 import
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';




//step 1 export
export default function Home() {
	//-refactoring banner.js 
	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/courses",
		label: "Enroll now!"
	}
	//-refactoring end
	return(
		<>	
		  	<Banner dataProp={data}/>  {/*dataProp user define*/}
          	<Highlights/>
        </>  
	)
}
