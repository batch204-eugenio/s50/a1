import Banner from "../components/Banner";

export default function Error()  {
	//-refactoring error.js 
	const data = {
		title: "404 - Not Found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"
	}
	//-refactoring end
	return(
		<>	
		  	<Banner dataProp={data}/>  {/*dataProp user define*/}
        </>  
	)
}


// //Error.js before refactoring...
// export default function Error() {
// 	return(
// 		<h1>404 - Page Not Found</h1>
// 	)
// }