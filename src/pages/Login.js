//ACTIVITY 51 create LOGIN page
// Page for Login
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
//111111111111111
import {Redirect} from 'react-router-dom';
//111111111111111
export default function Login() {
		const [email, setEmail] = useState("@gmail.com"); //you will see @gmail.com inside the place holder.
		const [password, setPassword] = useState("");
		const [isActive, setIsActive] = useState(false); //false is the default for isActive, we will use this for our button, activates and deactivates the button when the values are correct, the second button is disabled or grayed out
		const {user, setUser} = useContext(UserContext); //user holds the data provided by user.
		useEffect(()=> {
			if(email !== "" && password !== "") {
				setIsActive(true)
			} else {
				setIsActive(false)
			}
		}, [email, password]) //since we are monitoring the changes of these 3.

		function AuthenticateUser(e) { 
			e.preventDefault() //prevent the form behaviour.
			//from useState(localStorage.getItem("email"))
			//localStorage.setItme allows us to save a key/value pair to localStorage
			

			/*Activity create a fetch request inside this fx to allow users to log in, log in the response in the console, */
			fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
				method: "POST",
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
			})
			// localStorage.setItem("email", email)
			// //this setUser will set to localestorage and save on navbar...
			// //this will show  either login or logout
			// setUser({
			// 	email: email
			// })
			// //end useState()
			// setEmail("")
			// setPassword("")
			// alert("You are logged in!")
		

		}

		//we used 'onSubmit' 'event' to bind the function registerUser() to the form upon submission
		return (

			(user.email !== null) 
			?
			<Redirect to="/" />
			:

			<Form onSubmit={e => AuthenticateUser(e)}> 

				<Form.Group ControlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={e => setEmail(e.target.value)} //onchange event, the value of the target is now our 'new' 'useState' of 'setEmail' value, this is what the user types to the email
						required
					/>
					<Form.Text className="text-muted">	
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group ControlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password" //if you change this to "text" you will see the password instead of bullets/disc
						placeholder="Enter password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>

				{	isActive 
						? 
					<Button 
						className="mt-3"
						variant="primary" 
						type="submit" 
						id="submitBtn">
						Submit
					</Button> 
						:
					<Button 
						className="mt-3"
						variant="primary" 
						type="submit" 
						id="submitBtn"
						disabled>
						Submit
					</Button> 
				}

					{/*11111111111111111111111111111111111*/}
				{/*(user.email !== null) ? <Redirect to = "/"/> : */}
					{/*1111111111111111111111111111111111*/}

		
			</Form>
		)
}