//step 2 import
import {useEffect, useState} from 'react' //useState is GLOBAL
// import courseData from '../data/courseData'; //we comment this out we will use now the real DB
import CourseCard from '../components/CourseCard' //child
// import React from 'react';

//step 1 export
export default function Courses() {
	//******************************
	const [coursesData, setCoursesData] = useState([]);
	//******************************

	//check to see if the mock data was captured
	// console.log(courseData);
	// console.log(courseData[0]);
		//Props, is a shorthand for 'property' since components are considered as object in ReactJS.
		//Props is a way to pass data from a parent component to a child component.
		//Synonymous to function parameter
		//This is referred to as 'props drilling' or passing from parent to child and not the other way around

	//don't forget to encapsulate inside fragment <></>
	//courseProp is user defined, passing component from parent to child not the other way around.

	//'Courses' is the 'parent' of 'courseCard', courseCard is the 'child'

	//this 'courses' is an array of 3 courses from courseData
	//we are mapping the data from 'courseData.js', we are creating a new array.
	// const name = 'mark' u may pass this after {course}, <CourseCard courseProp = {course} pangalan = {name} key={course.id}/> 
	
	//******************************
	//from mongoDB //we started the terminal for s37-s41 where our course API is.
	//fetch by 'default' always makes a GET request, unless a different one is specified
	//ALWAYS add fetch request for getting data in a useEffect hook
	useEffect(()=> {
		//from mongoDB and s37-s41 make sure that port is listening.
		//changes to .env files are applied ONLY at build time (when starting the project locally/re start the project or terminal)
		console.log(process.env.REACT_APP_API_URL)
		fetch(`${process.env.REACT_APP_API_URL}/courses`) //this will be replaced by render/heroku URL/address once deployed.
		.then(res => res.json())
		.then(data => {
			//console.log(data) //this is fx scope and can't be passed on to 'map'
			setCoursesData(data) //this will run as a FX that will be passed on to 'map'
		})
	}, []) //empty so this will only run when the page loads.
	//******************************

	//data.map picks up data from DB/Course API
	const courses = coursesData.map(course => {
		return(
			<CourseCard courseProp = {course} key={course._id}/> //id with undescore bcoz this is the DB format
			// <CourseCard courseProp = {course} key={course.id}/> //id with no underscore bcoz this is the mockdata format
		)
	})
	return(
		<>
			<h1>Courses</h1>
			{courses} 
		</>
	)
}

//PROPS - parent to child is like, a sheet of paper as parent divided to 3 folds as its children.