//APP.JS is the PARENT COMPONENT

// STEP 1, IMPORT AREA
import {useState} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom' //the "as" keyword gives an 'alias' to the router components.
import './App.css';
import AppNavBar from './components/AppNavBar'; //components folder is for the components that we will reuse to make it organize.
import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register'; //so anything on Switch, what we will do is just to type localhost:3000/courses or /register or /login
//LOGIN ACTIVITY s51 start------------------------------------
import Login from './pages/Login';
//LOGIN ACTIVITY s51 end--------------------------------------
import Error from './pages/Error';
import {UserProvider} from './UserContext';
import Logout from './pages/Logout';

//import AppNavBar from AppNavBar.js folder.
/*day 3
All other components/pages will be contained in our main component: <app/>

*/


//STEP 2, MOUNTING AREA

function App() {
  //Single state (useState) that all our components will see, access and handle if the user is logged in or not. 
  //we will use 'localStorage.getItem("email)' inside the fx useState() instead of using "false"
  //the default state of user is from useState(localStorage.getItem("email"))
  const [user, setUser] = useState({
    email: localStorage.getItem("email")
  })
  //end of single state
  return (
   //calling AppNavBar from from AppNavBar.js folder.
   //<></> that encapsulates AppNavBar and Banner is called a fragment which ensures that adjacent JSX elements will be rendered to avoid any error.
   //no need to use (,) after <AppNAvBar/> and <Banner/>

   //the <></> upon creating a router we changed it to <Router></Router>
//THe UserProvider component has a value attribute that we can use to pass our user state to our components.
//The UserProvider will pass {user, setUser} to all inside of it such as router, appnavbar etc.
//UserProvider is a better way to pass data than 'props'
<UserProvider value={{user, setUser}}>  {/*so we added this UserProvider for our SINGLE state (useState*/}
  <Router>
    <>
      <AppNavBar/>
      <Container fluid>
{/*   Do not comment this out, we will use the Switch instead
      <>
          <Home/>
          <Courses/>
          <Register/>
          <Login/>
        </>  */}
        <Switch> 
          <Route exact path  = "/" component={Home}/>
          <Route exact path  = "/courses" component={Courses}/>
          <Route exact path  = "/register" component={Register}/>
          <Route exact path  = "/login" component={Login}/>
          <Route exact path  = "/logout" component={Logout}/>
          <Route component={Error}/> {/*this is like .catch or default of switch if no match*/}
        </Switch>   
      </Container>  
    </>  
  </Router>  
</UserProvider>    
  );
}
//we used react-router-dom@4.3.1 version here
//if we will switch to version 6 we will change <Switch></Switch> to <Routes></Routes>
//Switch is like the switch statement, it will run to find its match URL
//Switch and route, we will only need to attach the route to the URL
//example localhost:3000/ or /courses or /register or /login
//route is like an envelope that we used to send anything to someone.
//exact means to prevent 'partial matching', the route URL should be exact.

export default App;
