//UserContext.js

// related to SINGLE state (useState) that all our components will see, access and handle if the user is logged in or not. 
import React from 'react';


//Context and Provider

//Creates a React context object
//contains data that can be passed around to multiple props
//Think of it like a delivery container or a box.
//this is like an empty box.
const UserContext = React.createContext();

//A provider is what is used to distribute the context object to the component
//from the word 'provider' it provides something which is 'value={{user, setUser}}'
export const UserProvider = UserContext.Provider;

// no need for curly braces sa navbar since this is "DEFAULT" and we are only exporting ONE.
export default UserContext; 